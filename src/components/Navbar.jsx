import { useStore } from '@nanostores/react';
import { lang } from '../store/language';
import styles from '../styles/Navbar.module.scss';

import { navbarLanguage as data } from '../data/languageData';

export default function Navbar() {
    
    const $lang = useStore(lang);
    
    return (

        <nav className={styles.navbar}>
            <ul>
                
                <li><a href="#about">{ data[$lang].aboutMe }</a></li>
                <li><a href="#projects">{ data[$lang].projects }</a></li>
                <li><a href="#contact">{ data[$lang].contact }</a></li>
                
            </ul>
        </nav>

    )
}