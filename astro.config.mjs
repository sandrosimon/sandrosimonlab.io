import { defineConfig } from 'astro/config';

// https://astro.build/config
import react from "@astrojs/react";

// https://astro.build/config
export default defineConfig({
  integrations: [react()],
  /* sitemap: true, */
  site: 'https://sandrosimon.gitlab.io/',
  base: '/sandrosimon.gitlab.io',
  outDir: 'public',
  publicDir: 'static',
});